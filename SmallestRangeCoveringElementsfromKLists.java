import java.util.*;

class Solution {
    public int[] smallestRange(List<List<Integer>> nums) {
        PriorityQueue<Element> minHeap = new PriorityQueue<>((a, b) -> a.val - b.val);
        
        int max = Integer.MIN_VALUE;
        // Initialize the min heap with the first element of each list
        for (int i = 0; i < nums.size(); i++) {
            int val = nums.get(i).get(0);
            max = Math.max(max, val);
            minHeap.offer(new Element(val, i, 0));
        }
        
        int rangeStart = -1, rangeEnd = -1;
        int minRange = Integer.MAX_VALUE;
        
        while (minHeap.size() == nums.size()) {
            Element minElement = minHeap.poll();
            int minVal = minElement.val;
            int listIdx = minElement.listIdx;
            int nextIdx = minElement.idx + 1;
            
            if (max - minVal < minRange) {
                minRange = max - minVal;
                rangeStart = minVal;
                rangeEnd = max;
            }
            
            if (nextIdx < nums.get(listIdx).size()) {
                int nextVal = nums.get(listIdx).get(nextIdx);
                max = Math.max(max, nextVal);
                minHeap.offer(new Element(nextVal, listIdx, nextIdx));
            }
        }
        
        return new int[]{rangeStart, rangeEnd};
    }
    
    class Element {
        int val;
        int listIdx;
        int idx;
        
        public Element(int val, int listIdx, int idx) {
            this.val = val;
            this.listIdx = listIdx;
            this.idx = idx;
        }
    }
}
